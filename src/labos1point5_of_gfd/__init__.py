import argparse
import contextlib
import getpass
import sys
import yaml
import gfd

DEFAULT_GEONAMES_USERNAME = "cle_paris"

TRANSPORTS = dict(
    VP=("Voiture", 1),
    AV=("Avion", 1),
    AV2=("Avion", 1),
    BA=("Ferry", 1),
    # CA="Autocar - trajets intercités",  # Car-Navette/Shuttle
    CA=("Voiture", 1),  # Car-Navette/Shuttle
    CV=("Voiture", 2),  # Co-Voiturage/Carpooling
    VR=("Voiture", 1),  # Moto/2 wheeled vehicle
    TA=("Taxi", 1),
    TR2=("Train", 1),
    UR=("Métro", 1),  # Urbain/Public Transportation
    VL=("Voiture", 1),  # Véhicule location/Rental car
    VS=("Voiture", 1),  # Véhicule service/Company car
    VEL=None,  # Vélo
)


def output_missions_labos1point5(output, missions, travel_filter=None):
    output.write(
        f"# mission	Date de départ	Ville de départ	Pays de départ	Ville de destination	Pays de destination	Mode de déplacement	Nb de personnes dans la voiture	Aller Retour (OUI si identiques, NON si différents)	Motif du déplacement (optionnel)	Statut de l'agent (optionnel)"
    )
    travel_shown = set()
    for mission in missions:
        for travel in mission.travels:
            if travel_filter is not None and not travel_filter(travel):
                continue
            no = mission.gfd_id
            departure_date = travel.departure.datetime.strftime("%d/%m/%Y")
            departure_city = travel.departure.geocode.address
            departure_country = travel.departure.geocode.country_code
            destination_city = travel.destination.geocode.address
            destination_country = travel.destination.geocode.country_code
            transport = TRANSPORTS[travel.transport]
            if transport is None:
                continue
            transport_mode, nb_share = transport
            both_ways = "NON"
            reason = ""
            status = "Inconnu"
            nb_times = 1
            columns = [
                departure_date,
                departure_city,
                departure_country,
                destination_city,
                destination_country,
                transport_mode,
                nb_share,
                both_ways,
                reason,
                status,
                nb_times,
            ]
            columns_str = "\t".join(columns)
            key = f"{mission.id_agent},{columns_str}"
            if key in travel_shown:
                continue
            travel_shown.add(key)
            output.write(f"{no},{columns}\n")


def load_missions(config, code_service):
    return gfd.Gfd(
        **config["gfd"],
        geolocator=gfd.geolocator_cache(
            "geonames.cache", gfd.geolocator_geonames(config["geonames"]["username"])
        ),
        code_service=code_service,
    )


def init_config(config_filename):
    if config_filename:
        with open(config_filename) as config_file:
            config = yaml.safe_load(config_file)
    else:
        print("LDAP username: ", end="")
        gfd_username = input()
        gfd_password = getpass.getpass()
        print(f"Geonames username (default: {DEFAULT_GEONAMES_USERNAME}): ", end="")
        geonames_username = input()
        if geonames_username == "":
            geonames_username = DEFAULT_GEONAMES_USERNAME
        gfd = dict(username=gfd_username, password=gfd_password)
        geonames = dict(username=geonames_username)
        config = dict(gfd=gfd, geonames=geonames)
    return config


def make_travel_filter(years):
    if years is None:
        return None
    separator = years.find("..")
    if separator == -1:
        range_lowerbound = int(years)
        range_upperbound = range_lowerbound
    else:
        range_lowerbound = int(years[:separator])
        range_upperbound = int(years[separator + 2 :])

    def travel_filter(travel):
        return (
            range_lowerbound <= travel.departure.raw_datetime.year <= range_upperbound
        )

    return travel_filter


def handle_commandline():
    parser = argparse.ArgumentParser(
        prog="labos1point5_of_gfd",
        description="""Produce a "fichier de mission" (a TSV file) for Labos1point5 by scrapping data from "portail mission" (GFD).""",
    )
    parser.add_argument("-c", "--config", help="configuration file")
    parser.add_argument("-y", "--years", help="filter by years")
    parser.add_argument("-o", "--output", help="output file")
    parser.add_argument("-s", "--service", help="code service")
    args = parser.parse_args()
    config = init_config(args.config)
    missions = load_missions(config, args.service).missions(old=True)
    travel_filter = make_travel_filter(args.years)
    with open(args.output, "w") if args.output else contextlib.nullcontext(
        sys.stdout
    ) as output:
        output_missions_labos1point5(output, missions, travel_filter=travel_filter)


if __name__ == "__main__":
    handle_commandline()
