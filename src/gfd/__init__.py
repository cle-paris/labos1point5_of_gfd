import datetime
import io
import json
import typing
import requests
import lxml.etree
import timezonefinder
import pytz
import geocoder

URL_GFD = "https://gfd.inria.fr"
URL_GFD_LIST_MISSIONS = f"{URL_GFD}/pv2/liste/Om.jsp"
URL_GFD_GET_MISSION = f"{URL_GFD}/pv2/fiche/Omi.jsp"


def unwrap_singleton(l):
    assert len(l) == 1
    return l[0]


def document_of_response(response):
    response.raise_for_status()
    return lxml.etree.parse(io.StringIO(response.text), lxml.etree.HTMLParser())


def title_of_html_document(document):
    return unwrap_singleton(document.xpath("/html/head/title/text()"))


def extract_travels(document):
    scripts = document.xpath("//script/text()")
    script_travel = next(
        script for script in scripts if script.startswith("var trajets;")
    )
    travel_string = script_travel[
        script_travel.index("'") + 1 : script_travel.rindex("'")
    ]
    travel_json = travel_string.encode("utf-8").decode("unicode_escape")
    return json.loads(travel_json)


def geolocator_geonames(key):
    def geolocator(city, country):
        try:
            prefix_city = city[: city.index("(")]
        except KeyError:
            prefix_city = city
        while True:
            try:
                results = geocoder.geonames(
                    prefix_city, country=country, key=key, maxRows=10
                )
                if len(results) >= 1:
                    break
            except Exception:
                pass
            try:
                prefix_city = city[: prefix_city.rindex(" ")]
            except ValueError:
                raise Exception(f"Unknown city: {city}, {country}")
        try:
            result = next(result for result in results if result.address == prefix_city)
        except StopIterator:
            result = results[0]
        return result

    return geolocator


def geolocator_cache(cache_file, geolocator):
    import shelve

    db = shelve.open(cache_file)

    def geolocator_cached(city, country):
        key = f"{city},{country}"
        try:
            result = db[key]
        except KeyError:
            result = geolocator(city, country)
            db[key] = result
        return result

    return geolocator_cached


class Gfd:
    def __init__(
        self,
        code_service=None,
        username=None,
        password=None,
        session=None,
        geolocator=None,
    ):
        if session is None:
            session = requests.session()
        self.__session = session
        self.__geolocator = geolocator
        self.__code_service = code_service
        response = session.get(URL_GFD)
        document = document_of_response(response)
        title = title_of_html_document(document)
        if title == "Central Authentication Service":
            execution = unwrap_singleton(
                document.xpath("//input[@name='execution']/@value")
            )
            data = dict(
                username=username,
                password=password,
                execution=execution,
                _eventId="submit",
                geolocation="",
            )
            response = session.post(response.url, data=data)
            document = document_of_response(response)
            title = title_of_html_document(document)
        if title == "Gestion des Frais de Déplacements":
            self.__management = True
        elif title == "Portail Missions":
            self.__management = False
        else:
            raise Exception("Authentication failure")

    def missions(self, old=False):
        if self.__management:
            self.__session.get(
                f"https://gfd.inria.fr/astre/fd/cadre/SetCadreTravail.jsp?codeColl=INRIA&codeService={self.__code_service}-0"
            )
            params = dict(content="fr.gfi.gfd.controller.astre.fd.mission.OmiLin")
            response = self.__session.get(
                "https://gfd.inria.fr/astre/fd/csl/GetListe.jsp", params=params
            )
            document = response.json()
            nb_pages = int(document["jPagine"]["nbPage"])
            gfd_ids = []
            for i in range(0, nb_pages):
                gfd_ids.extend((value[0] for value in document["jListe"].values()))
                if i < nb_pages - 1:
                    params["NumPage"] = str(i + 2)
                    response = self.__session.get(
                        "https://gfd.inria.fr/astre/fd/csl/GetListe.jsp", params=params
                    )
                    document = response.json()

        else:
            if old:
                params = dict(fini="true")
            else:
                params = {}
            response = self.__session.get(URL_GFD_LIST_MISSIONS, params=params)
            document = document_of_response(response)
            gfd_ids = document.xpath("//div[@class='liste om']/div/@data-gfd-id")
        return [
            Mission(self.__session, self.__geolocator, gfd_id) for gfd_id in gfd_ids
        ]


class Mission:
    def __init__(self, session, geolocator, gfd_id):
        self.__session = session
        self.__gfd_id = gfd_id
        params = dict(id=gfd_id)
        response = self.__session.get(URL_GFD_GET_MISSION, params=params)
        document = document_of_response(response)
        self.__id_agent = int(
            unwrap_singleton(
                document.xpath("//div[@id='fiche-mission']/@data-gfd-id-agent")
            )
        )
        self.__travels = [
            Travel(geolocator, desc) for desc in extract_travels(document)
        ]

    @property
    def gfd_id(self):
        return self.__gfd_id

    @property
    def id_agent(self):
        return self.__id_agent

    @property
    def travels(self):
        return self.__travels


class Travel:
    def __init__(self, geolocator, desc):
        self.__departure = Point(geolocator, desc, "Depart")
        self.__destination = Point(geolocator, desc, "Arrivee")
        self.__transport = desc["codeTransport"]

    @property
    def departure(self):
        return self.__departure

    @property
    def destination(self):
        return self.__destination

    @property
    def transport(self):
        return self.__transport


class Point:
    def __init__(self, geolocator, desc, suffix):
        self.__desc = desc
        self.__geolocator = geolocator
        self.__suffix = suffix
        self.__geocode = None
        self.__datetime = None
        self.__raw_datetime = None

    def __get(self, property_name):
        return self.__desc[property_name + self.__suffix]

    @property
    def geocode(self):
        if self.__geocode is not None:
            return self.__geocode
        city = self.__get("libelleCommune")
        country = self.__get("codePays")
        self.__geocode = self.__geolocator(city, country)
        return self.__geocode

    @property
    def raw_datetime(self):
        if self.__raw_datetime is not None:
            return self.__raw_datetime
        self.__raw_datetime = datetime.datetime.strptime(self.__get("jour"), "%d/%m/%Y")
        return self.__raw_datetime

    @property
    def datetime(self):
        if self.__datetime is not None:
            return self.__datetime
        date = datetime.datetime.strptime(self.__get("jour"), "%d/%m/%Y")
        location = self.geocode
        timezone = timezonefinder.TimezoneFinder().timezone_at(
            lng=float(location.lng), lat=float(location.lat)
        )
        self.__datetime = pytz.timezone(timezone).localize(
            datetime.datetime(
                date.year,
                date.month,
                date.day,
                hour=int(self.__get("heure")),
                minute=int(self.__get("minute")),
            )
        )
        return self.__datetime
