# `labos1point5_of_gfd`

Produce a "fichier de mission" (a TSV file) for Labos1point5 by
scrapping data from "portail mission" (GFD).

## Installation

The tool can be installed together with its dependencies via `pip`, either from `gitlab`

```bash
pip install git+https://gitlab.inria.fr/cle-paris/labos1point5_of_gfd.git
```

or from a local copy of the repository.

```bash
git clone https://gitlab.inria.fr/cle-paris/labos1point5_of_gfd
cd labos1point5_of_gfd
pip install -e .
```

## Usage

```bash
$ labos1point5_of_gfd -y 2022 -o fichier_de_mission.tsv
LDAP username: <enter your LDAP username>
Password: <hidden password>
Geonames username (default: cle_paris): <leave blank or enter a login>
```

If `-y 2022` is omitted, all past missions are printed.
A range can be specified as `-y 2020..2023`.
If `-o fichier_de_mission.tsv` is omitted, the TSV table is written on
standard output.
For non-interactive use, a configuration file can be provided with
`-c config.yml`, where `config.yml` has the following form.

```yaml
gfd:
  username: ldap_login
  password: ???
geonames:
  username: cle_paris
```

## Limitations

The optional column `Statut de l'agent` is left blank: for each
mission, the script knows the `gfd-id-agent` (for instance, for me
Thierry Martinez, it is `23132`), but I don't know how to get a status
from that numbeUr.

The optional column `Motif du déplacement` is left blank: for each
mission, there is a `Motif`, but it is a free-form text, and I don't
know how to map this to a value among `[ "Etude terrain",
"Colloque-Congrès", "Séminaire", "Enseignement", "Collaboration",
"Visite", "Administration de la recherche", "Autre", "Inconnu" ]`.

GFD distinguishes between the following transport modes, that are all
mapped to the transport mode `Voiture` recognized by Labos1point5.

- `CV`: carpooling (we put 2 in the column `Nb de personnes dans la voiture`)
- `VR`: moto/2 wheeled vehicle
- `VL`: rental car
- `VS`: company car

GFD knows about bicycle (`VEL`), but Labos1point5 does not.
`VEL` travels are removed from the list.

The import tool in Labos1point does not recognize `Autocar - trajets
intercités`: we substitute them by `Voiture` as a workaround.

## Heuristics

Geographic names are resolved by requesting
[geonames.org](geonames.org), according to labos1point5
recommendation. Note that the offline dump `cities500.txt` misses some
places like _Sophia Antipolis_.

In GFD, cities are written as `CITY NAME (POSTAL CODE OR COUNTRY
NAME)`: the contents of the parentheses is dropped since it breaks
search.  Country name is redundant with the country code provided;
postal code could be useful to resolve ambiguities in France but it is
not currently used.

Some places are abbreviated in GFD such as `ROISSY AEROPORT CH DE GAU
(95)`. `ROISSY AEROPORT CH DE GAU` has [no hit on
geonames.org](https://www.geonames.org/search.html?q=ROISSY+AEROPORT+CH+DE+GAU).
The heuristics is to try prefixes by removing right-most words one
after the other, until we found a place known by
[geonames.org](geonames.org): in this case, [`ROISSY
AEROPORT`](https://www.geonames.org/search.html?q=ROISSY+AEROPORT).

We request [geonames.org](geonames.org) for the 10 first rows: if
there is an exact match among them, we use it.  Otherwise, we take the
first result. For instance,
[the first result for `Bloomington`](https://www.geonames.org/search.html?q=bloomington)
is `Minneapolis-St. Paul-Bloomington, MN-WI`,
whereas [`Bloomington`](https://www.geonames.org/4254679/bloomington.html)
appears second.

The tool prints the same travel (identical places and date and time)
for the same agent only once, to remove duplicated missions.

## Account on geonames.org

Geographic data are requested from [geonames.org](geonames.org),
according to labos1point5 recommendation.  The account `cle_paris` has
been registered with API access enabled, which should be enough to
handle all our requests thanks to caching strategy (cache is stored in
`geonames.cache.db`).  The password is not needed to access the API,
but members of the CLE Paris can find it on the following page:

https://gitlab.inria.fr/cle-paris/cle-paris-int/-/blob/main/logins

You may want to use another account if we run out of usage limits
(1000 requests per hour). Don't forget to enable API access for your
account.

